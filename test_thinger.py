"""
Tests for thinger.
"""
import unittest

from thinger import thingify

THINGIFIED_ALPHABET = """
      __   __   __   ___  ___  __                                    __   __   __   __   __  ___                        __ 
 /\  |__) /  ` |  \ |__  |__  / _` |__| |    | |__/ |     |\/| |\ | /  \ |__) /  \ |__) /__`  |  |  | \  / |  | \_/ \ /  / 
/~~\ |__) \__, |__/ |___ |    \__> |  | | \__/ |  \ |___  |  | | \| \__/ |    \__X |  \ .__/  |  \__/  \/  |/\| / \  |  /_ 
"""
SPACED_WORDS = """
 __   __        __   ___  __           __   __   __   __  
/__` |__)  /\  /  ` |__  |  \    |  | /  \ |__) |  \ /__` 
.__/ |    /~~\ \__, |___ |__/    |/\| \__/ |  \ |__/ .__/ 
"""


class TestThinger(unittest.TestCase):

    def test_thingify_upper(self):
        self.assertEqual(
            thingify("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), THINGIFIED_ALPHABET)

    def test_thingify_lower(self):
        self.assertEqual(
            thingify("abcdefghijklmnopqrstuvwxyz"), THINGIFIED_ALPHABET)

    def test_spaced_words(self):
        self.assertEqual(thingify("Spaced Words."), SPACED_WORDS)
