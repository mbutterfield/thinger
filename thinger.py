"""
Thinger.
"""

LETTERS_ART = {
    ' ': '\n   \n   \n   \n',
    'a': '\n     \n /\\  \n/~~\\ \n',
    'b': '\n __  \n|__) \n|__) \n',
    'c': '\n __  \n/  ` \n\\__, \n',
    'd': '\n __  \n|  \\ \n|__/ \n',
    'e': '\n ___ \n|__  \n|___ \n',
    'f': '\n ___ \n|__  \n|    \n',
    'g': '\n __  \n/ _` \n\\__> \n',
    'h': '\n     \n|__| \n|  | \n',
    'i': '\n  \n| \n| \n',
    'j': '\n     \n   | \n\\__/ \n',
    'k': '\n     \n|__/ \n|  \\ \n',
    'l': '\n     \n|    \n|___ \n',
    'm': '\n      \n |\\/| \n |  | \n',
    'n': '\n     \n|\\ | \n| \\| \n',
    'o': '\n __  \n/  \\ \n\\__/ \n',
    'p': '\n __  \n|__) \n|    \n',
    'q': '\n __  \n/  \\ \n\\__X \n',
    'r': '\n __  \n|__) \n|  \\ \n',
    's': '\n __  \n/__` \n.__/ \n',
    't': '\n___ \n |  \n |  \n',
    'u': '\n     \n|  | \n\\__/ \n',
    'v': '\n     \n\\  / \n \\/  \n',
    'w': '\n     \n|  | \n|/\\| \n',
    'x': '\n    \n\\_/ \n/ \\ \n',
    'y': '\n    \n\\ / \n |  \n',
    'z': '\n__ \n / \n/_ \n',
}


def thingify(text: str) -> str:
    """
    Thingify text.
    """
    result = ["", "", "", "", ""]
    for char in text:
        try:
            art_char = LETTERS_ART[char.lower()]
        except KeyError:
            continue
        for i, line in enumerate(art_char.split("\n")):
            result[i] += line

    return "\n".join(result)
